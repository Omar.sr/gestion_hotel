/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metiers;

import Daos.Dao;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author omar
 */
public class GestionClients {
     ExecuteStatement Es ;
     
     public GestionClients()
     {
         Es = new ExecuteStatement();
     }
     
     public int ajouter_Client(String cin,String nom , String prenom ,String  tel , String email,String adresse,String ville ,java.util.Date Datenaissance,int codePostal)
     {
         String  req="INSERT INTO `clients` (`CIN`, `NOM`, `PRENOM`, `TEL`, `EMAIL`, `ADRESSE`, `VILLE`, `DATENAISSANCE`, `CODEPOSTALE`) "
                 + "VALUES ('"+cin+"','"+nom+"','"+prenom+"', '"+tel+"', '"+email+"','"+adresse+"', '"+ville+"','"+new java.sql.Date(Datenaissance.getTime())+"', "+codePostal+")";
    
        // String req2="INSERT INTO `compteclients`(`LOGINCL`, `CIN`, `PASSWORD`) VALUES ('"+cin+"_"+nom+"','"+cin+"','"+new java.sql.Date(Datenaissance.getTime())+"')";
         return Es.ExecuteUpdate(req);
     }
     public int ajouterCompteClient(String login,String cin, String password)
     {
         String req2="INSERT INTO `compteclients`(`LOGINCL`, `CIN`, `PASSWORD`) VALUES ('"+login+"','"+cin+"','"+password+"')";
         return Es.ExecuteUpdate(req2);
     }
     public int delete_client(String cin)
     { String req="delete from clients where cin='"+cin+"'";
         
     return Es.ExecuteUpdate(req);
     }
     /**
     *
     * Recherche du client selon sont Cin 
     */
      public ResultSet RechercherCLient (String cin)
    {
               String req="select cin , nom , prenom from clients  where cin='"+cin+"'";
           return Es.ExecuteQuery(req);
    }
    
    /**
     * 
     * Afficher les Clients ayant atteint la fin de leur sejour Selon La date Actuel
     *
     * */
    public ResultSet ExpirationCL()
    {
        
           
               java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
               String req="Select c.cin , c.nom , c.prenom , r.numeroch , tel , r.datefin from clients as c , reserver as r where c.cin = r.cin and datefin= '"+date+"'";
               
               return Es.ExecuteQuery(req);
     
    }
    
public ResultSet ClientByDate(Date d,Date f){
 
      
            
          String  req="select CIN,NOM,PRENOM,TEL,EMAIL,ADRESSE,DATENAISSANCE as DateNaissance ,NUMEROCH as N°Chambre from CLIENTS,RESERVER "
                    + "where RESERVER.DATEDEBUT='"+d+"' and RESERVER.DATEFIN='"+f+"'AND CLIENTS.CIN=RESERVER.CIN ";
   
            return Es.ExecuteQuery(req);
       
   }
/**
 * rechrche by cin
 * 
 * */
public ResultSet ClientByCin_Reserver(String Cin ){
      

            String req ;
            
            req="select NOM,PRENOM,TEL,EMAIL,ADRESSE,DATENAISSANCE as DateNaissance,NUMEROCH as N°Chambre ,R.DateDebut,R.DateFin  from CLIENTS C,RESERVER R "
                    + "where CLIENTS.CIN='"+Cin+"' AND CLIENTS.CIN=RESERVER.CIN ";
            return Es.ExecuteQuery(req);
      
       

}

/**
 * 
* afficher les service d un client
*/
public ResultSet ServiceByClient(String c)
    {
        
   
            String req ;
            
            req="select IDS,DESIGNATION,PRIX from DEMANDER D,SERVICES S WHERE S.IDS=D.IDS "
                    + "AND D.CIN='"+c+"' ";
          
             return Es.ExecuteQuery(req);
        }
     
 public boolean authentification_Client ( String login , String pwd)
    {
       
            String req="Select * from clients where Login = '"+login+"' and Password = '"+pwd+"'";
            
         try {
             return Es.ExecuteQuery(req).first();
         } catch (SQLException ex) {
             Logger.getLogger(GestionComptePersonnel.class.getName()).log(Level.SEVERE, null, ex);
         }

            return false;
    }

public int AjouterComplainte(String cin,String description)
{
    String req="INSERT INTO `complaintes`(`CIN`, `DESCRIPTION`) VALUES (['"+cin+"','"+description+"')";
return Es.ExecuteUpdate(req);
}

public int Paiement(String Numeroc ,String Cin,Date dateexp,String ccv)
{
    String req="INSERT INTO `compte_banquaire`(`NUMEROC`, `CIN`, `DATEEXPIRATION`, `CCV`) VALUES ('"+Numeroc+"','"+Cin+"','"+new java.sql.Date(dateexp.getTime())+"','"+ccv+"')";
    return Es.ExecuteUpdate(req);
}


}
