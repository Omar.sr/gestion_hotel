/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metiers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asus
 */
public class GestionPersonnel {
     private ExecuteStatement Es ;
    
    public GestionPersonnel()
    {
        Es = new ExecuteStatement();
    }
    public Personnel ChercherPersonnel(String cin,String role )
    {
        String req="select * from Personnel where cin_e='"+cin+"';";
        ResultSet rs=Es.ExecuteQuery(req);
         try {
             while(rs.next())
             {
                 String cin_p=rs.getString(1);
                 String nom=rs.getString(2);
                String prenom= rs.getString(3);
                 Date datenaissance =rs.getDate(4);
                String email= rs.getString(5);
                String telephone= rs.getString(6);
               String sexe=  rs.getString(7);
                 String adresse =rs.getString(8);
                int code_postal= rs.getInt(9);
                 
                 Personnel pr=new Personnel(cin_p,role,nom,prenom,datenaissance,email,telephone,sexe,adresse,code_postal);
                 return pr;
             }
         } catch (SQLException ex) {
             Logger.getLogger(GestionPersonnel.class.getName()).log(Level.SEVERE, null, ex);
         }
        return null;
    }
            
    public void ajouterPersonnel(String cin_e,String nom,String prenom,java.util.Date Date_naissance, String email , String telephone,String sexe,String adresse,int codepostal )
    {
       String req= "INSERT INTO personnel VALUES ('"+cin_e+"','"+nom+"','"+prenom+"','"+new java.sql.Date(Date_naissance.getTime())+"','"+email+"','"+telephone+"','"+sexe+"','"+adresse+"','"+codepostal+"')";
       
        Es.ExecuteUpdate(req);
     }  
    
    public void ajouterEmploye(String cin_e,String nom,String prenom,java.util.Date Date_naissance, String email , String telephone,String sexe,String adresse,int codepostal )
    {
     String req= "INSERT INTO employe VALUES ('"+cin_e+"','"+nom+"','"+prenom+"','"+new java.sql.Date(Date_naissance.getTime())+"','"+email+"','"+telephone+"','"+sexe+"','"+adresse+"','"+codepostal+"')";
       this.ajouterPersonnel(cin_e, nom, prenom, Date_naissance, email, telephone, sexe, adresse, codepostal);
       Es.ExecuteUpdate(req);
    }
          
    public void ajouterReceptioniste(String cin_r,String nom,String prenom,java.util.Date Date_naissance, String email ,String telephone,String sexe,String adresse,int codepostal )
    {
       
       String req= "INSERT INTO receptionistes VALUES ('"+cin_r+"','"+nom+"','"+prenom+"','"+new java.sql.Date(Date_naissance.getTime())+"','"+email+"','"+telephone+"','"+sexe+"','"+adresse+"','"+codepostal+"')";
       this.ajouterEmploye(cin_r, nom, prenom, Date_naissance, email, telephone, sexe, adresse, codepostal);
       Es.ExecuteUpdate(req);
    }  
    
    public void ajouterGerant(String cin_gr,String nom,String prenom,java.util.Date Date_naissance, String email ,String telephone,String sexe,String adresse,int codepostal )
    {
       String req= "INSERT INTO gerant VALUES ('"+cin_gr+"','"+nom+"','"+prenom+"','"+new java.sql.Date(Date_naissance.getTime())+"','"+email+"','"+telephone+"','"+sexe+"','"+adresse+"','"+codepostal+"')";
       System.out.println("ajouterpersonnel");
       this.ajouterEmploye(cin_gr, nom, prenom, Date_naissance, email, telephone, sexe, adresse, codepostal);
       System.out.println("ajoutergerant");
       Es.ExecuteUpdate(req);
    }  
    
    public void ajouterDirecteur(String cin_d,String nom,String prenom,java.util.Date Date_naissance, String email , String  telephone,String sexe,String adresse,int codepostal )
    {
     String req= "INSERT INTO directeur VALUES ('"+cin_d+"','"+nom+"','"+prenom+"','"+new java.sql.Date(Date_naissance.getTime())+"','"+email+"','"+telephone+"','"+sexe+"','"+adresse+"','"+codepostal+"')";
       
     this.ajouterPersonnel(cin_d, nom, prenom, Date_naissance, email, telephone, sexe, adresse, codepostal);
     
       Es.ExecuteUpdate(req);
    }  
    
    // Ra 7aydt ID dial directeur cle etrangere mn employe
}
