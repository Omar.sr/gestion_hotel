/***********************************************************************
 * Module:  Gerant.java
 * Author:  omar
 * Purpose: Defines the Class Personnel
 ***********************************************************************/
package Metiers;
import java.util.*;


public class Personnel {

   private java.lang.String cinE;
   
   private java.lang.String nom;

   private java.lang.String prenom;

   private java.util.Date dateNaissance;

   private java.lang.String email;

   private String telephone;

   private java.lang.String sexe;

   private java.lang.String adresse;
  
   private String role ; 
    
    public String getCinE() {
        return cinE;
    }

    public void setCinE(String cinE) {
        this.cinE = cinE;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public long getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(long codePostal) {
        this.codePostal = codePostal;
    }


   public long codePostal;

    public Personnel(String cinE,String role , String nom, String prenom, Date dateNaissance, String email, String telephone, String sexe, String adresse, long codePostal) {
        this.cinE = cinE;
        this.nom = nom;
        this.prenom = prenom;
       this.dateNaissance = dateNaissance;
        this.role=role;
        this.email = email;
        this.telephone = telephone;
        this.sexe = sexe;
        this.adresse = adresse;
        this.codePostal = codePostal;
    }
    

}