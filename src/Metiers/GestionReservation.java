/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metiers;

import Daos.Dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author omar
 */
public class GestionReservation {
     ExecuteStatement es ;
     
    public  GestionReservation()
     {
         es = new ExecuteStatement();
     }
     /**
      * 
      * Ajouts d'une reservation
      * @param numeroCh
      * @param cin
      * @param cin_Recep
      * @param datedebut
      * @param datefin
      * @return 
      * pour  reservation en ligne cin_Recep est null 
      */
      public int Ajouter_reservation(int numeroCh,String cin,String cin_Recep,java.util.Date datedebut,java.util.Date datefin)
      { 
                String req="INSERT INTO reserver (`CIN_R`, `CIN`, `NUMEROCH`, `DATEDEBUT`, `DATEFIN`) "
                        + "VALUES ('"+cin_Recep+"','"+cin+"', '"+numeroCh+"', '"+new java.sql.Date(datedebut.getTime())+"', '"+ new java.sql.Date(datefin.getTime())+"')";
              return es.ExecuteUpdate(req);
      }
          
 /**
  * 
  * 
  * -Rechercher Les information de la Reservation d'une chambre par un client
   * 
 */ 
    public ResultSet RechercherRes (String cin , int numC )
    {
        
              java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
              String req="Select numeroch as N°chambre,cin, datedebut , datefin from reserver where cin = '"+cin+"' and numeroch = "+numC+" and datedebut < '"+date+"' and datefin >= '"+date+"' ";
           return es.ExecuteQuery(req);
    }
      
       
      
      
      
      
}
