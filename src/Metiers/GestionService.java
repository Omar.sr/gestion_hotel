/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metiers;


import java.util.Date;
import Daos.Dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asus
 */
public class GestionService{
    ExecuteStatement es;
    
    public GestionService()
    {
        es = new ExecuteStatement();
    }
    
  
    // Rechercher Les Service D'un client Actuel D'une Chambre
    public ResultSet ServicesClientActuel (String cin ,int numch)
    {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        
        String query = "Select description , prix , d.date from service AS s , demander AS d , reserver AS r where" + 
                "s.ids=d.ids and d.id_r=r.id_r and r.cin = '" + cin + "' and r.numeroch = '" + numch + "' and datedebut < '" + date + "' and datfin >= '" + date + "'";
        return es.ExecuteQuery(query);
    }
    
    // Prix Totale Des service d'un Client Actuel
    public ResultSet PrixTotaleService (String cin ,int numch)
    {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        
        String query = "Select sum(prix) from service AS s , demander AS d , reserver AS r where" + 
                "s.ids=d.ids and d.id_r=r.id_r and r.cin = '" + cin + "' and r.numeroch = '" + numch + "' and datedebut < '" + date + "' and datfin >= '" + date + "'";
        return es.ExecuteQuery(query);
    }
     
    // Les info sur la reservation d'un client actuel d une chambre
    public ResultSet InfoReservation (String cin ,int numch) // pour l affichage des service
    {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        
        String query = "Select numeroch , datedebut , datefin  from reserver where" + 
                "cin = '" + cin + "' and numeroch = '" + numch + "' and datedebut < '" + date + "' and datfin >= '" + date + "'";
        return es.ExecuteQuery(query);
    }
    
    //Ajouter un service
    public void AjouterService(int id , float prix , String designation)
    {
        String query= "INSERT INTO `services`(`IDS`, `PRIX`, `DESIGNATION`) VALUES ('"+id+"','"+prix+"','"+designation+"')";
        es.ExecuteUpdate(query);
    }
    
    //Supprimer un Service 
    
    public void SupprimerService(int id)
    {
        String query= "Delete from services where ids="+id+"";
        es.ExecuteUpdate(query);
    }
    
    public Vector<String> getServices()
     {
          Vector<String> services=new Vector<String>();
         String req="select * from services";
         ResultSet rs=es.ExecuteQuery(req);
        try {
            while(rs.next())
            {
                services.add(rs.getInt(1)+"-"+rs.getString(3));
                
            }
            return services;
        } catch (SQLException ex) {
            Logger.getLogger(GestionService.class.getName()).log(Level.SEVERE, null, ex);
        }
         
         return null; 
     }
    
}